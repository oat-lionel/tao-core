define(['jquery', 'ui', 'ui/uploader'], function($, ui, uploader){
      
    $('.file-upload').uploader({
        upload: true,
        read : true
    });
 
});
